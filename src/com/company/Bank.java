package com.company;

public class Bank {
    private String bankName;
    private Currency[] currencies;

    public Bank(String bankName, Currency[] currencies) {
        this.bankName = bankName;
        this.currencies = currencies;
    }

    public String getBankName() {
        return bankName;
    }

    public Currency[] getCurrencies() {
        return currencies;
    }


}
