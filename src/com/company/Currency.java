package com.company;

public class Currency {
    private String currencyName;
    private float sellCource;
    private float buyCource;

    public Currency(String currencyName, float sellCource, float buyCource) {
        this.currencyName = currencyName;
        this.sellCource = sellCource;
        this.buyCource = buyCource;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public float getSellCource() {
        return sellCource;
    }


    public float getBuyCource() {
        return buyCource;
    }

    public static boolean isCurrencyValid(String customersCurrency){
        if (customersCurrency.equalsIgnoreCase("UAH")) {
           return true;
        } else if (customersCurrency.equalsIgnoreCase("RUB") ||
                customersCurrency.equalsIgnoreCase("USD") ||
                customersCurrency.equalsIgnoreCase("EUR")) {
            return true;
        } else {
            System.out.println("smth went wrong. Please try again");
            return false;
        }
    }

}
