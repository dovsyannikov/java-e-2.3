package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Task2();
    }

    public static void Task2() {
        // 2.3* На её основе сделать программу для конвертации трёх валют в трёх разных
        // банках по двум курсам: покупка и продажа. Возможные курсы валют и имена банков
        // известны заранее. Кросс-курс не предусмотрен (менять можно валюту на гривну или
        // гривну на валюту). Название банка, валюты и операции (покупка/продажа) для
        // конвертации вводятся с консоли.
        Bank[] banks = Generator.generator();

        Scanner scanner = new Scanner(System.in);
        System.out.println("What currency do you have (USD, UAH, RUB, EUR)");
        String customersCurrency = scanner.nextLine();
        System.out.println("What sum do you have");
        float customersSum = Float.parseFloat(scanner.nextLine());
        System.out.println("What bank do you prefer? (pumb, oshad, privat)?");
        String bankName = scanner.nextLine();
        String preferedCurrency = null;
        if (customersCurrency.equalsIgnoreCase("UAH")){
            System.out.println("To which currency do you want to change your money? (USD, RUB, EUR, UAH)");
            preferedCurrency = scanner.nextLine();
        }
        Bank bankUserWant = Generator.usersBank(bankName, banks);
        Currency[] currencies = bankUserWant.getCurrencies();

        if (Currency.isCurrencyValid(customersCurrency)){
            if (customersCurrency.equalsIgnoreCase("UAH")) {
                for (Currency currency : currencies) {
                    if (currency.getCurrencyName().equalsIgnoreCase(preferedCurrency)) {
                        System.out.println(String.format("After changing your %.2f %s will become: %.2f %s",
                                customersSum,
                                customersCurrency,
                                customersSum / currency.getBuyCource(),
                                preferedCurrency));
                    }
                }
            } else {
                for (Currency currency : currencies) {
                    if (currency.getCurrencyName().equalsIgnoreCase(customersCurrency)){
                        System.out.println(String.format("After changing your %.2f %s will become: %.2f grivnas",
                                customersSum,
                                customersCurrency,
                                customersSum * currency.getSellCource()));
                    }
                }
            }
        }
    }
}
