package com.company;

public class Generator {
    public static Bank[] generator(){
        Bank [] banks = new Bank[3];
        {
            Currency [] currencies = new Currency[3];
            currencies[0] = new Currency("USD", 27.0f, 27.2f);
            currencies[1] = new Currency("EUR", 30.1f, 30.3f);
            currencies[2] = new Currency("RUB", 0.39f, 0.4f);
            banks[0] = new Bank("privat", currencies);
        }
        {
            Currency [] currencies = new Currency[3];
            currencies[0] = new Currency("USD", 27.1f, 27.4f);
            currencies[1] = new Currency("EUR", 30.4f, 30.9f);
            currencies[2] = new Currency("RUB", 0.49f, 0.67f);
            banks[1] = new Bank("oshad", currencies);
        }
        {
            Currency [] currencies = new Currency[3];
            currencies[0] = new Currency("USD", 27.6f, 28.2f);
            currencies[1] = new Currency("EUR", 32.1f, 32.3f);
            currencies[2] = new Currency("RUB", 0.59f, 0.6f);
            banks[2] = new Bank("pumb", currencies);
        }
        return banks;
    }
    public static Bank usersBank (String userBank, Bank[] banks){
        for (Bank bank : banks){
            if (bank.getBankName().equalsIgnoreCase(userBank)) {
                return bank;
            }
        }
        return null;
    }
}
